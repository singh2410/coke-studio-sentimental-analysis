#!/usr/bin/env python
# coding: utf-8

# # Twitter Scrapped Data:EDA & Sentiment Prediction
# ## By- Aarush Kumar
# ### Dated: April 11,2022

# In[1]:


import numpy as np 
import pandas as pd
import re # for regular expressions
import pandas as pd 
pd.set_option("display.max_colwidth", 200)
import numpy as np 
import matplotlib.pyplot as plt 
import seaborn as sns
import string
import nltk # for text manipulation
import warnings 
warnings.filterwarnings("ignore", category=DeprecationWarning)
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


df = pd.read_csv("/home/aarush100616/Downloads/Projects/Coke Studio EDA/Data/coke_studio.csv")


# In[3]:


df


# In[4]:


print('Dataset size:',df.shape)
print('Columns are:',df.columns)


# In[5]:


# use the drop columns function to streamline the dataset
df = df.drop(columns=['id', 'time','user_id','username','conversation_id','created_at','timezone', 'name', 'place', 'mentions', 'urls', 'photos', 'replies_count', 'likes_count', 'cashtags', 'link', 'retweet','retweets_count', 'quote_url', 'video', 'near', 'geo', 'source', 'user_rt_id', 'user_rt', 'retweet_id', 'reply_to', 'retweet_date'])
df.head(10)


# In[6]:


print('Dataset size:',df.shape)
print('Columns are:',df.columns)
df.info()


# In[7]:


#convert data to datetime and strings for manipulation.
df["tweet"]= df["tweet"].astype(str)
df['date']= pd.to_datetime(df['date'], infer_datetime_format=True)
df.info()


# ## Data Pre-processing

# In[8]:


import string
import nltk
from nltk.corpus import stopwords
from nltk import re


# In[9]:


MIN_YEAR = 1900
MAX_YEAR = 2100


# In[10]:


def get_url_patern():
    return re.compile(
        r'(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))'
        r'[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})')


# In[11]:


def get_emojis_pattern():
    try:
        # UCS-4
        emojis_pattern = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
    except re.error:
        # UCS-2
        emojis_pattern = re.compile(
            u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')
    return emojis_pattern


# In[12]:


def get_hashtags_pattern():
    return re.compile(r'#\w*')
def get_single_letter_words_pattern():
    return re.compile(r'(?<![\w\-])\w(?![\w\-])')
def get_blank_spaces_pattern():
    return re.compile(r'\s{2,}|\t')
def get_twitter_reserved_words_pattern():
    return re.compile(r'(RT|rt|FAV|fav|VIA|via)')
def get_mentions_pattern():
    return re.compile(r'@\w*')
def is_year(text):
    if (len(text) == 3 or len(text) == 4) and (MIN_YEAR < len(text) < MAX_YEAR):
        return True
    else:
        return False


# In[13]:


class TwitterPreprocessor:
    def __init__(self, text: str):
        self.text = text
    def fully_preprocess(self):
        return self             .remove_urls()             .remove_mentions()             .remove_hashtags()             .remove_twitter_reserved_words()             .remove_punctuation()             .remove_single_letter_words()             .remove_blank_spaces()             .remove_stopwords()             .remove_numbers()
    def remove_urls(self):
        self.text = re.sub(pattern=get_url_patern(), repl='', string=self.text)
        return self
    def remove_punctuation(self):
        self.text = self.text.translate(str.maketrans('', '', string.punctuation))
        return self
    def remove_mentions(self):
        self.text = re.sub(pattern=get_mentions_pattern(), repl='', string=self.text)
        return self
    def remove_hashtags(self):
        self.text = re.sub(pattern=get_hashtags_pattern(), repl='', string=self.text)
        return self
    def remove_twitter_reserved_words(self):
        self.text = re.sub(pattern=get_twitter_reserved_words_pattern(), repl='', string=self.text)
        return self
    def remove_single_letter_words(self):
        self.text = re.sub(pattern=get_single_letter_words_pattern(), repl='', string=self.text)
        return self
    def remove_blank_spaces(self):
        self.text = re.sub(pattern=get_blank_spaces_pattern(), repl=' ', string=self.text)
        return self
    def remove_stopwords(self, extra_stopwords=None):
        if extra_stopwords is None:
            extra_stopwords = []
        text = nltk.word_tokenize(self.text)
        stop_words = set(stopwords.words('english'))
        new_sentence = []
        for w in text:
            if w not in stop_words and w not in extra_stopwords:
                new_sentence.append(w)
        self.text = ' '.join(new_sentence)
        return self
    def remove_numbers(self, preserve_years=False):
        text_list = self.text.split(' ')
        for text in text_list:
            if text.isnumeric():
                if preserve_years:
                    if not is_year(text):
                        text_list.remove(text)
                else:
                    text_list.remove(text)

        self.text = ' '.join(text_list)
        return self
    def lowercase(self):
        self.text = self.text.lower()
        return self


# In[14]:


# Clean tweets and append to new column
tweets = df['tweet']
clean_tweets = []
for tweet in tweets:
    c = TwitterPreprocessor((tweet))
    c.fully_preprocess()
    c = c.text
    clean_tweets.append(c)
    
df['clean_tweets'] = clean_tweets 
df.head(5)


# ## Story Generation and Visualization from Tweets

# In[15]:


all_words = ' '.join([text for text in df['clean_tweets']])
from wordcloud import WordCloud
wordcloud = WordCloud(width=800, height=500, random_state=21, max_font_size=110).generate(all_words)
plt.figure(figsize=(10, 7))
plt.imshow(wordcloud, interpolation="bilinear")
plt.axis('off')
plt.show()


# ## Sentiment Analysis Using VADER

# In[16]:


import nltk
nltk.download("vader_lexicon")


# In[17]:


from nltk.sentiment.vader import SentimentIntensityAnalyzer


# In[18]:


# create analyzer object 
analyzer = SentimentIntensityAnalyzer()
# get a list of scores and plot
scores = [analyzer.polarity_scores(tweet)['compound'] for tweet in df['clean_tweets']]
plt.hist(scores, bins=20)


# In[19]:


sentiment = df['clean_tweets'].apply(lambda x: analyzer.polarity_scores(x))
df = pd.concat([df,sentiment.apply(pd.Series)],1)
df.head(5)


# ## Analyzing Sentiment

# In[20]:


df.describe()


# In[21]:


df.index = pd.to_datetime(df['date'])
df = df.sort_index()
df['mean'] = df['compound'].expanding().mean()
df['rolling'] = df['compound'].rolling('1d').mean()


# In[22]:


import datetime as dt
fig = plt.figure(figsize=(20,5))
ax = fig.add_subplot(111)
ax.scatter(df['date'],df['compound'], label='Tweet Sentiment')
ax.plot(df['date'],df['rolling'], color ='r', label='Rolling Mean')
ax.plot(df['date'],df['mean'], color='g', label='Expanding Mean')
ax.set(title='Coke studio Tweets over Time', xlabel='Date', ylabel='Sentiment')
ax.legend(loc='best')
fig.tight_layout()
plt.xticks(
    rotation=45, 
    horizontalalignment='right',
    fontweight='light',
    fontsize='x-large'  
)
plt.show()


# In[23]:


fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(111)
ax.set(title='Coke studio Tweets Sentiment Score', xlabel='Compund Sentiment Score', ylabel='Frequency')
sns.distplot(df['compound'], bins=15, ax=ax)
plt.show()

